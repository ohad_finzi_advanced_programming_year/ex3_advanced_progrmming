#include "Vector.h"


//HELP FUNCTION

/*
The function creates a new array using the given number, and copies all the old array elements into the new one
Input: a new size for the array
*/
void Vector::realloc(int n)
{
	int* newAry = new int[n];  //creates a new array with the new _capacity
	for (int i = 0; i < this->_capacity; i++)  //a loop that runs on the old array to move all its elements to the new array
	{
		newAry[i] = this->_elements[i];
	}

	this->_capacity = n;  //changes the _capactity element according to n

	delete[] this->_elements;  //deletes all the dynamic memory allocated for the creation of the old array
	this->_elements = newAry;  //points the Vector class array to point on the new created array
}


//A

/*
The functions construct the current Vector class by initializng all its elements according to the given element
Input: the wanted size of the _elements array 
*/
Vector::Vector(int n)
{
	if (n < 2)  //checks if the given array length is lower than 2
	{ 
		n = 2;  //if true, the size will be 2
	}
	this->_elements = new int[n];  //allocates the dynamic memory for the _elemets array according to the given length

	//initializing all the Vector class elements
	this->_capacity = n; 
	this->_resizeFactor = n;
	this->_size = 0;
}


/*
The functions diconstructs the current Vector class by deleting all the allocated dynamic memory used in the class
*/
Vector::~Vector()
{
	delete[] this->_elements;
	this->_elements = nullptr;
}


/*
The function returns the _size element of the current Vector class
*/
int Vector::size() const
{
	return this->_size;
}


/*
The function returns the _capacity element of the current Vector class
*/
int Vector::capacity() const
{
	return this->_capacity;
}


/*
The function returns the _resizeFactor element of the current Vector class
*/
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}


/*
The function checks if the current Vector class _elements array is empty - _size = 0
Output: |true - _size = 0| or |false - _size > 0|
*/
bool Vector::empty() const
{
	bool ans = true;

	if (this->_size)
	{
		ans = false;
	}

	return ans;
}


//B

/*
The function adds a new value to the current Vector array. if the Vector ran out of free space (_size == _capacity), the functions will create a new array with an extra space for the given value
Input: the value to add to the current Vector array
*/
void Vector::push_back(const int& val)
{
	if (this->_size == this->_capacity)  //checks if the vector isnt full
	{
		realloc(this->_capacity + this->_resizeFactor);  //creates a new array in the size of current array _capacity + _resizeFactor
	}

	//puts the given value in the array
	this->_elements[this->_size] = val;
	this->_size++;
}


/*
The functions returns the last entered element of the current Vector class array and deletes it from the array
Output: the last entered element of the current Vector class array. if the array is empty, the return value will be -9999
*/
int Vector::pop_back()
{
	int returnVal = 0;

	if (!this->empty())  //checks if the array isnt empty
	{
		this->_size -= 1;  //lowers the number of available elements in the array
		returnVal = this->_elements[this->_size];
		this->_elements[this->_size] = NULL;
	}
	else
	{
		std::cout << "error:pop from empty vector" << std::endl;
		returnVal = -9999;  //a sign to the user that the current Vector class array is empty
	}

	return returnVal;
}


/*
The function creates a new array if the given number is bigger than the current array capacity
Input: the new size of the array ig bigger than the current array _capacity
*/
void Vector::reserve(int n)
{
	if (this->_capacity < n)  //checks if the given number is bigger than the current array _capacity 
	{
		int newSize = 0;

		while (newSize < n)  //creates the new size of the array using _resizeFactor
		{
			newSize += this->_resizeFactor; 
		}

		realloc(newSize);  //creates the new array
	}
	//else - nothing to change
}


/*
The function creates a new array according to given size
Input: the new size of the array
*/
void Vector::resize(int n)
{
	if (n < this->_capacity && n > 0)  //checks if the size if smaller than the current array size
	{
		this->_capacity = n;
		realloc(n);  //creates the new array according to the given size

		if (this->_size >= n)
		{
			this->_size = n;
		}
	}
	else
	{
		reserve(n);  //if the given size is bigger, the function reserve will create the new array using _resizeFactor
	}
}


/*
The function sets all the current array elements to the given value
Input: the new value of all the elements of the current value
*/
void Vector::assign(int val)
{
	for (int i = 0; i < this->_size; i++)  //runs on the current array
	{
		this->_elements[i] = val;
	}
}


/*
The function creates a new array if the given size if bigger than the current array size, and sets all the new elements to given val
Input: the size of the new array if bigger than the current array size, and the val of the new created elements of the new array
*/
void Vector::resize(int n, const int& val)
{
	int oldCapacity = this->_capacity;  //saves the capacity before the change

	resize(n);  //creating a new array according to the given size
	 
	for (int i = oldCapacity; i < this->_capacity; i++)  //a loop that runs through all the new elements of the given array
	{
		this->_elements[i] = val;
	}
}


//C

/*
The functions construct the current Vector class by initializng all its elements according to the given Vector class
Input: the Vector class to copy
*/
Vector::Vector(const Vector& other)
{
	//shallow copy fields
	this->_capacity = other._capacity;  
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	
	//deep copy the dynamic fields - array and pointers
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}
}


/*
Thr function puts all the values of the given Vector class to the current Vector class (similar to the deep copy constructor function)
Input: the Vector class to copy
Ouput: the current Vector class with the same elements as the given Vector class
*/
Vector& Vector::operator=(const Vector& other)
{
	if (this == &other)  //checks if the current Vector class is already equal to the given Vector class
	{
		return *this;  //if true, the function is done by returning the current Vector class
	}

	delete[] this->_elements;  //deletes the current Vector array  

	//shallow copy fields
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;

	//deep copy the dynamic fields - array and pointers
	this->_elements = new int[this->_capacity];
	for (int i = 0; i < this->_capacity; i++)
	{
		this->_elements[i] = other._elements[i];
	}

	return* this;
}


//D

/*
The function allows the user to use [] operators on the current Vector class like a regular array
Input: the wanted index to get from the current array
Output: if the given index is valid (n is above 0 and not bigger than size), than the return will be the n index of the current array
but if n is invalid, the return will be the first index of the current array
*/
int& Vector::operator[](int n) const
{
	if (n >= this->_size || n < 0)
	{
		std::cout << "the given index is bigger than this Vector capacity!" << std::endl;
		return this->_elements[0];
	}

	return this->_elements[n];
}

